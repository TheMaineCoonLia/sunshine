<?php

/*
 * ************************************************************************* *
 * Copyright (C) Nicolaj Christoffer Rørvig - All Rights Reserved            *
 * Unauthorized copying of this file, via any medium is strictly prohibited  *
 * Proprietary and confidential                                              *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      *
 *                                                                           *
 * Written by Nicolaj Christoffer Rørvig <roe@roehost.net>, January 2015     *
 * ************************************************************************* *
 */

/**
 *
 * This interface should be extended by objects you want to use as a event.
 *
 * @author Nicolaj Christoffer Rørvig
 * @since 0.2.0
 * @access public
 * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
 */
interface Event
{

    /**
     *
     * This function is used to get the name of the event.
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return string the name of the event
     */
    public function get_name( );

    /**
     *
     * This function should store the array of handlers that have been through the event. <br />
     * The Event Manager will populate it when the event is dispatched
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return array the handlers that have been through the event
     *
     */
    public function get_handlers( );

}