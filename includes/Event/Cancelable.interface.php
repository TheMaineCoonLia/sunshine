<?php

/*
 * ************************************************************************* *
 * Copyright (C) Nicolaj Christoffer Rørvig - All Rights Reserved            *
 * Unauthorized copying of this file, via any medium is strictly prohibited  *
 * Proprietary and confidential                                              *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      *
 *                                                                           *
 * Written by Nicolaj Christoffer Rørvig <roe@roehost.net>, January 2015     *
 * ************************************************************************* *
 */

/**
 *
 * This interface is used to make a event cancelable
 *
 * @author Nicolaj Christoffer Rørvig
 * @since 0.2.0
 * @access public
 * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
 *
 */
interface Cancelable
{

    /**
     *
     * This function is used to tell if the event have been cancelled
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return bool wether or not the event is cancelled
     *
     */
    public function is_cancelled( );

    /**
     *
     * This function is used to define wether or not an event should be cancelled
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $cancelled wether or not you want to cancel the event. Should be true or false
     *
     */
    public function set_cancelled( $cancelled );

}