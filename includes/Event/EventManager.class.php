<?php

/*
 * ************************************************************************* *
 * Copyright (C) Nicolaj Christoffer Rørvig - All Rights Reserved            *
 * Unauthorized copying of this file, via any medium is strictly prohibited  *
 * Proprietary and confidential                                              *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      *
 *                                                                           *
 * Written by Nicolaj Christoffer Rørvig <roe@roehost.net>, January 2015     *
 * ************************************************************************* *
 */

/**
 *
 * This class is used to manage all event related things
 *
 * @author Nicolaj Christoffer Rørvig
 * @since 0.2.0
 * @access public
 * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
 *
 */
class EventManager
{

    /**
     *
     * This variable stores the registered listeners
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @var array the listeners
     *
     */
    private $listeners = array( );

    /**
     *
     * Construct a new event manager
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     *
     */
    public function __construct( )
    {

    }

    /**
     *
     * This function is used to register a listener
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $event the name of the event you want to listen for
     * @param callable $listener the listener to be executed when an event of the type selected is being dispatched
     * @param string $priority the priority of the listener. Available Prioritys: Lightest, Light, Normal, Heavy, Heaviest, Monitor
     * @param bool $ignoreCancelled wether or not to ignore events that have been cancelled by other listeners
     *
     */
    public function register_listener( $event, callable $listener, $priority = "Normal", $ignoreCancelled = true )
    {

        //Add the listener to the other registered listeners
        $this->get_listeners( )[ $event ][ $priority ][ ] = array(

            "ignoreCancelled" => $ignoreCancelled,
            "callable" => $listener

        );

    }

    /**
     *
     * This method is used to check if an event have 1 or more listeners registered
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $event the name of the event to check for listeners for
     * @return bool true if 1 or more listeners have been registered for the event. False if not
     *
     */
    public function has_listener( $event )
    {

        //Return wether or not there's been listeners registered to that event
        return isset( $this->get_listeners( )[ $event ] );

    }

    /**
     *
     * This function is used to dispatch an event
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param Event $event the event to dispatch
     */
    public function dispatch_event( Event $event )
    {

        //Check if there's any listeners registered to the event
        if( $this->has_listener( $event->get_name( ) ) )
        {

            //Loop through all the available priorities
            foreach( $this->get_priorities( ) as $priority )
            {

                //Check if the event has any listeners registered with the current priority
                if( isset( $this->get_listeners( )[ $event->get_name( ) ][ $priority ] ) )
                {

                    //It does! Now, take all the listeners registered to the priority
                    $listeners = $this->get_listeners( )[ $event->get_name( ) ][ $priority ];

                    //Loop through the listeners
                    foreach( $listeners as $listener )
                    {

                        //Check if the event is a cancelable one
                        if( $event instanceof Cancelable)
                        {

                            //Check if the event is cancelled and that the listener doesn't want cancelled events
                            if( $event->is_cancelled( ) && $listener[ "ignoreCancelled" ] )
                            {

                                //Continue to next listener
                                continue;

                            }

                        }

                        //Check that the priority isn't the Monitor priority
                        if( $priority != "Monitor" )
                        {

                            //Setup try/catch. Just in case a listener messes soemthing up
                            try
                            {

                                //Dispatch the event to the current listener!
                                call_user_func( $listener[ "callable" ], $event );

                            }
                            catch( Exception $e)
                            {

                                //Ignore the error

                            }

                        }
                        else
                        {

                            //Do a try catch for this as cloning may throw an exception
                            try
                            {

                                //Dispatch a cloned event to the current listener with monitor priority.
                                call_user_func( $listener[ "callable" ], clone $event );

                            }
                            catch(Exception $e)
                            {

                                //Ignore this error

                            }

                        }

                    }

                }

            }

        }

    }

    /*
     *
     * Getters & Setters
     *
     */

    /**
     *
     * This function is used to get all the registered listeners
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return array the registered listeners
     *
     */
    public function get_listeners( )
    {

        //Return the registered listeners
        return $this->listeners;

    }

    public function get_priorities( )
    {

        return array(

            "Lightest",
            "Light",
            "Normal",
            "Heavy",
            "Heaviest",
            "Monitor"

        );

    }

}