<?php

/*
 * ************************************************************************* *
 * Copyright (C) Nicolaj Christoffer Rørvig - All Rights Reserved            *
 * Unauthorized copying of this file, via any medium is strictly prohibited  *
 * Proprietary and confidential                                              *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      *
 *                                                                           *
 * Written by Nicolaj Christoffer Rørvig <roe@roehost.net>, January 2015     *
 * ************************************************************************* *
 */

/**
 *
 * This is the primary class for the whole application.
 *
 * @author Nicolaj Christoffer Rørvig
 * @since 0.0.1
 * @access public
 * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
 *
 */
class Sunshine
{

    /**
     *
     * This variable stores the instance of the sunshine application
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.0.1
     * @access private
     * @static
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @var Sunshine the sunshine instance
     *
     */
    private static $instance;

    /**
     *
     * This constant stores the version of sunshine you're running
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.0.1
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @var Sunshine the sunshine application version
     *
     */
    const VERSION = "0.4.0";

    /**
     *
     * This variable stores the event manager in use by the application
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @var EventManager the event manager
     *
     */
    private $event_manager;

    /**
     *
     * This variable stores the replaceable tags available to the application
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.3.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @var array the array of replaceable tags
     *
     */
    private $replaceable_tags = array( );

    /**
     *
     * This variable stores the files loaded by the application
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @var array the array of loaded files
     *
     */
    private $loaded_files = array(

        //The files that's only been loaded once
        "once" => array( ),
        //Everything else
        "all" => array( )

    );

    /**
     *
     * This constructor is used to construct a new sunshine application object.
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.0.1
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @throws \Exception This will be thrown if the an instance of the application already exists
     *
     */
    public final function __construct( )
    {

        //Check if an instance have already been created
        if( self::$instance != null )
        {

            //Okay an instance exists. Throw an error complaining about it
            throw new \Exception( "A instance have already been created! Please call get_instance( )!" );

        }

        //Setup the instance
        self::$instance = $this;

        //Setup the default replaceable tags
        $this->register_replaceable_tag( 'inc', dirname( dirname( __FILE__ ) ) . "/" );
        $this->register_replaceable_tag( 'root', dirname( dirname( dirname( __FILE__ ) ) ) . "/" );

        //Load the files needed to setup the event manager
        if( !$this->load_file_set( "event" ) )
        {

            throw new Exception("Unable to load event file set!");

        }

        //Setup the event manager
        $this->set_event_manager( new EventManager( ) );

    }

    /**
     *
     * This function is used to register a tag
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.3.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $name string the name of the tag. Please note that the name will be converted to full uppercase
     * @param $value mixed the value of the tag
     * @throws Exception this is thrown if the tag already exists
     *
     */
    public function register_replaceable_tag( $name, $value )
    {

        //Make sure the name is uppercase
        $name = strtoupper( $name );

        //Check if a tag with that name exists
        if( $this->get_replaceable_tag( $name ) != null )
        {

            //Throw an exception
            throw new Exception( "A tag with that name already exists!" );

        }

        //Set the tag
        $this->set_replaceable_tag( $name, $value );

        //Define the tag with a SUNSHINE_ prefix
        define( "SUNSHINE_" . $name, $value );

    }

    /**
     *
     * This function is used to get the value of replaceable tag by it's name. <br >
     * Null will be returned if the tag doesn't exist
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.3.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $name string name of the tag you're looking for
     * @return mixed the value of the tag if the tag have been registered. Null if not
     *
     */
    public function get_replaceable_tag( $name )
    {

        //Make sure the name is uppercase
        $name = strtoupper( $name );

        //Check if the replaceable tag exists
        if( isset( $this->get_replaceable_tags( )[ $name ] ) )
        {

            //Return the replaceable tag
            return $this->get_replaceable_tags( )[ $name ];

        }

        //Return null since a tag assigned to the provided name wasn't found
        return null;

    }

    /**
     *
     * This function is used to replace all the tags in a string with their respective values
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $string string the string you want the tags to be replaced
     * @return string the string that was originally passed to the function but with the tags replaced with their respective values
     *
     */
    public function replace_tags( $string )
    {

        //Loop through all of the available tags
        foreach( $this->get_replaceable_tags( ) as $name => $value )
        {

            //Replace the tag
            $string = str_replace( "%SUNSHINE_" . $name . "%", $value, $string );

        }

        //Return the string
        return $string;

    }

    /**
     *
     * This function is used to load a file
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $file string the file to load
     * @param bool $once wether or not to load the file with require_once
     * @return bool wether or not the file was loaded successfully
     *
     */
    public function load_file( $file, $once = false )
    {

        //Replace possible replaceable tags
        $file = $this->replace_tags( $file );

        //Check wether or not the file exists
        if( !file_exists( $file ) )
        {

            //The file doesn't exist! Return false
            return false;

        }

        //Check if we should load the file with require_once
        if( $once )
        {

            //Load the file
            require_once $file;

            //Add the file to the list
            $this->add_loaded_file( $file, $once );

            //Tell the caller that we've successfully loaded the file
            return true;

        }

        //Load the file
        require $file;

        //Add the file to the list
        $this->add_loaded_file( $file, $once );

        //Tell the caller that we've successfully loaded the file
        return true;


    }

    /**
     *
     * This function is used to load a file set
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $file_set string the file set to load
     * @throws \Exception this is thrown if the file set doesn't exist
     *
     */
    public function load_file_set( $file_set )
    {

        //Check if the file set exists
        if( !$this->file_set_exists( $file_set ) )
        {

            //Throw an error
            throw new Exception( "Unkown file set " + $file_set );

        }

        foreach( $this->get_file_set( $file_set ) as $file => $type )
        {

            switch( $type )
            {

                case "interface":

                    $file .= ".interface";

                    break;

                case "class":

                    $file .= ".class";

                    break;

                default:

                    break;

            }

            $file .= ".php";
            $file = $this->replace_tags( $file );

            if( !$this->load_file( $file ) )
            {

                return false;

            }

        }

        return true;

    }

    /**
     *
     * This function is used to check if a file set exists
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $file_set string the file set to check exists
     * @return bool true if the file set exists, false if not
     *
     */
    public function file_set_exists( $file_set )
    {

        //Return wether or not the file set exists
        return isset( $this->get_file_sets( )[ $file_set ] );

    }

    /*
     *
     * Getters & Setters
     *
     */

    /**
     *
     * This function is used to get the instance of the sunshine application. <br />
     * A new instance will be crated if one doesn't exist
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.0.1
     * @access public
     * @static
     * @final
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return Sunshine the sunshine instance. If a instance doesn't exist a new one will be created
     *
     */
    public static final function get_instance( )
    {

        //Check if an instance have been defined
        if( self::$instance == null )
        {

            //Okay, an instance haven't been defined. Let's create one
            new Sunshine( );

        }

        //Return the defined instance
        return self::$instance;

    }

    /**
     *
     * This function is used to get the event manager in use by the application
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return EventManager the event manager instance
     *
     */
    public function get_event_manager( )
    {

        //Return the event manager
        return $this->event_manager;

    }

    /**
     *
     * This function is used to set the event manager the application should use
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.2.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param EventManager $event_manager the event manager you want to the application to use
     *
     */
    private function set_event_manager( EventManager $event_manager )
    {

        //Set the event manager instance
        $this->event_manager = $event_manager;

    }

    /**
     *
     * This function is used to get the replaceable tags available to the application
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.3.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return array the replaceable tags available to the application
     *
     */
    public function get_replaceable_tags( )
    {

        //Return the replaceable tags
        return $this->replaceable_tags;

    }

    /**
     *
     * This function is used to set the a specific tag
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.3.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $name string the name of the tag to set
     * @param $value mixed the value of the tag to set
     *
     */
    private function set_replaceable_tag( $name, $value )
    {

        //Set the tag
        $this->replaceable_tags[ $name ] = $value;

    }

    /**
     *
     * This function is used to get the list of loaded files
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return array an array with the loaded files
     *
     */
    public function get_loaded_files( )
    {

        return $this->loaded_files;

    }

    /**
     *
     * This function is used internally to add a file to the list of other loaded files
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access private
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $file string the file that's been loaded
     * @param $once bool wether or not the file have been loaded with require_once
     *
     */
    private function add_loaded_file( $file, $once )
    {

        //Check if the file have been loaded with require_once
        if( $once )
        {

            //Check if we've loaded the file before
            if( $this->get_loaded_files( )[ "once" ][ $file ] )
            {

                //Increment the load count by 1
                $this->get_loaded_files( )[ "once" ][ $file ] = $this->get_loaded_files( )[ "once" ][ $file ] + 1;

            }
            else
            {

                //Set the load count to 1
                $this->get_loaded_files( )[ "once" ][ $file ] = 1;

            }

        }
        else
        {

            //Add the file to the list of loaded files
            $this->get_loaded_files( )[ "all" ][ ] = $file;

        }

    }

    /**
     *
     * This function is used to get a list of file sets available
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @final
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @return array the array of file sets
     *
     */
    public final function get_file_sets( )
    {

        return array(

            "event" => array(

                "%SUNSHINE_INC%Event/Event" => "interface",
                "%SUNSHINE_INC%Event/Cancelable" => "interface",
                "%SUNSHINE_INC%Event/EventManager" => "class"

            )

        );

    }

    /**
     *
     * This function is used to get a file set by it's name
     *
     * @author Nicolaj Christoffer Rørvig
     * @since 0.4.0
     * @access public
     * @copyright Copyright (c) 2015 and Onwards, Nicolaj Christoffer Rørvig. All rights reserved.
     * @param $file_set string the file set to get
     * @throws \Exception this is thrown if the file set doesn't exist
     * @return array the file set
     *
     */
    public final function get_file_set( $file_set )
    {

        //Check if the file set actually exists
        if( !$this->file_set_exists( $file_set ) )
        {

            //Throw an error
            throw new Exception( "The file set " + $file_set + " doesn't exist!" );

        }

        //Return the file set
        return $this->get_file_sets( )[ $file_set ];

    }

}